
// Просчет Vh в зависимости от высоты экрана
window.addEventListener('resize', () => {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});


//
$('body').on('scroll', function() {
  scrollPage.call(this);
})

//Функция смены элементов
function scrollPage() {

  let scrolled = this.scrollTop || document.documentElement.scrollTop;
  let windowHeight = document.documentElement.clientHeight;

  // Первое изображение на мониторе
  if (scrolled > windowHeight*0.4) {
    document.getElementById('first_slide').style.opacity = '0';
    document.getElementById('specific-page-count-one').style.top = '120px';
    document.getElementById('my-paroller').style.marginTop = '-210px';
    document.getElementById('first-nav-circle').style.backgroundColor = 'rgba(255,255,255,0)';
  } else if (scrolled <= windowHeight*0.4) {
    document.getElementById('first_slide').style.opacity = '100';
    document.getElementById('specific-page-count-one').style.top = '0';
    document.getElementById('my-paroller').style.marginTop = '0';
    document.getElementById('first-nav-circle').style.backgroundColor = 'rgba(255,255,255,1)';
  }

  // Второе изображние на мониторе
  if (scrolled > windowHeight*0.5 && scrolled < windowHeight*1.5) {
    document.getElementById('second_slide').style.opacity = '100';
    document.getElementById('specific-page-count-two').style.top = '0';
    document.getElementById('my-paroller').style.marginTop = '-210px';
    document.getElementById('second-nav-circle').style.backgroundColor = 'rgba(255,255,255,1)';
  } else if (scrolled < windowHeight*0.5 || scrolled > windowHeight*1.5) {
    document.getElementById('second_slide').style.opacity = '0';
    document.getElementById('specific-page-count-two').style.top = '120px';
    document.getElementById('second-nav-circle').style.backgroundColor = 'rgba(255,255,255,0)';
  } 
    
  // Третье изображение на мониторе
  if (scrolled > windowHeight*1.5 && scrolled < windowHeight*2.4) {
    document.getElementById('third_slide').style.opacity = '100';
    document.getElementById('specific-page-count-three').style.top = '0';
    document.getElementById('my-paroller').style.marginTop = '-430px';
    document.getElementById('third-nav-circle').style.backgroundColor = 'rgba(255,255,255,1)';
  } else if (scrolled < windowHeight*1.5 || scrolled > windowHeight*2.4) {
    document.getElementById('third_slide').style.opacity = '0';
    document.getElementById('specific-page-count-three').style.top = '120px';
    document.getElementById('third-nav-circle').style.backgroundColor = 'rgba(255,255,255,0)';
  }

  // Монитор
  if (scrolled > windowHeight*2.4) {
    $('.offer_text_block').each(function(){
      $(this).css({ opacity: 0 })})

    // document.getElementById('offer_text_block').style.opacity = '0';
    document.getElementById('screen').style.opacity = '0'
    document.getElementById('my-paroller').style.marginTop = '-660px';
  } else if (scrolled < windowHeight*2.4) {
    $('.offer_text_block').each(function(){
      $(this).css({ opacity: 100 })})
    // document.getElementById('offer_text_block').style.opacity = '100';
    document.getElementById('screen').style.opacity = '100';
  }

  // Убирает кнопку request_a_quote
  if (scrolled > windowHeight*4.5) {
    $('.request_a_quote_button').each(function(){
      $(this).css({ opacity: 0 });
    }) 
    // document.getElementById('requestAQuoteButton').style.opacity = '0';
  } else if (scrolled <= windowHeight*4.5) {
    $('.request_a_quote_button').each(function(){
      $(this).css({ opacity: 100 });
    }) 

  }


  // передвигаем счетчик страниц -- 4
  if (scrolled > windowHeight*2.4 && scrolled < windowHeight*3.5) {
    document.getElementById('specific-page-count-four').style.top = '0';
    document.getElementById('fourth-nav-circle').style.backgroundColor = 'rgba(255,255,255,1)';
  } else if (scrolled < windowHeight*2.4 || scrolled > windowHeight*3.5) {
    document.getElementById('specific-page-count-four').style.top = '120px';
    document.getElementById('fourth-nav-circle').style.backgroundColor = 'rgba(255,255,255,0)';
  }

  // передвигаем счетчик страниц -- 5
  if (scrolled > windowHeight*3.5 && scrolled < windowHeight*4.5) {
    document.getElementById('specific-page-count-five').style.top = '0';
    document.getElementById('my-paroller').style.marginTop = '-870px';
    document.getElementById('fifth-nav-circle').style.backgroundColor = 'rgba(255,255,255,1)';
  } else if (scrolled < windowHeight*3.5 || scrolled > windowHeight*4.5) {
    document.getElementById('specific-page-count-five').style.top = '120px';
    document.getElementById('fifth-nav-circle').style.backgroundColor = 'rgba(255,255,255,0)';
  }

  // передвигаем счетчик страниц -- 6
  if (scrolled > windowHeight*4.5) {
    document.getElementById('specific-page-count-six').style.top = '0';
    document.getElementById('my-paroller').style.marginTop = '-1148px';
    document.getElementById('sixth-nav-circle').style.backgroundColor = 'rgba(255,255,255,1)';
  } else if (scrolled < windowHeight*4.5 || scrolled > windowHeight*5.2) {
    document.getElementById('specific-page-count-six').style.top = '120px';
    document.getElementById('sixth-nav-circle').style.backgroundColor = 'rgba(255,255,255,0)';
  }
};


//Функция перемещения по экрану
$('a').click(function(){
  if($(this).data('btn') === 'websiteSection'){
    let elementClick = $(this).attr("href");

    let destination = $(elementClick).offset().top;

    if($.browser.safari) {
      $('body').animate({ scrollTop: destination }, 200);   
    } else {
      $('html').animate({ scrollTop: destination }, 200)
    }
    return false;
  }
})


// Переключение языка
$('.lang-btn').click(function() {
  if ($(this).data('lang') === "ua") {
    $(this).addClass('language-active');
    $(this).siblings().removeClass('language-active');
    $('.en').each(function() {
      $(this).addClass('hidden')
    })
    $('.ua').each(function() {
      $(this).removeClass('hidden');
    })

    $('#inp_name').attr('placeholder', 'Ім\'я');
    $('#company').attr('placeholder', 'Компанія');
    $('#message').attr('placeholder', 'Повідомлення')
    $('#send-btn').text('НАДІСЛАТИ')


  } else if ($(this).data('lang') === "en") {
    $(this).addClass('language-active');
    $(this).siblings().removeClass('language-active');
    $('.en').each(function() {
      $(this).removeClass('hidden')
    })
    $('.ua').each(function() {
      $(this).addClass('hidden')
    })

    $('#inp_name').attr('placeholder', 'Name')
    $('#company').attr('placeholder', 'Company')
    $('#message').attr('placeholder', 'Message')
    $('#send-btn').text('SEND')
  } 
})


//Анимация отправки формы
$('#send-btn').on('click', function(){
  console.log('click')
  $('.contact_form').addClass('hidden');
  $('.thank-you').removeClass('hidden');
  // $('.contact_form').css('display','none');
})

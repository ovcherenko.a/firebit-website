$(document).ready(function() {
    $(".single-item").slick({
      dots: false,
      vertical: false,
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 4,
      autoplay: true,
      arrows: true,
      mobileFirst: true
    });
  });